angular
	.module('chatroom')
	.config(chatConfig);

function chatConfig($routeProvider, $locationProvider){
	$routeProvider
   		.when('/', {
    		templateUrl: 'login.html',
    		controller: 'loginController',
		  })
      .when('/chat/:roomId', {
        templateUrl: 'chatroom.html',
        controller: 'chatController',
      });
}