// service
angular
    .module('chatroom')
    .service('dataService', dataService);

function dataService() {
	this.getFirebaseUrl = function(){
		return 'https://incandescent-inferno-8910.firebaseio.com/';
	}
}