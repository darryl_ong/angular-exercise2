angular
	.module('chatroom')
	.controller('loginController',['$rootScope','$scope','$location',loginController]);

function loginController(rootScope, scope, location){
	scope.roomId = 'demo';
	scope.nameError = '';
	scope.roomIdError = '';

	scope.doLogin = function () {
		scope.nameError = '';
		scope.roomIdError = '';
		if(scope.validate()){
			rootScope.name = scope.name;
			location.path('/chat/'+scope.roomId);
		}
	};

	//inhouse validation coz not using forms
	scope.validate = function(){
		var validName = /^[a-zA-Z0-9_]+$/.test(scope.name);
		var validRoomId = /^[a-zA-Z]+$/.test(scope.roomId);
		if(!validName){
			scope.nameError = 'Name should only consist of letters, numbers or underscore';
		}
		if(!validRoomId){
			scope.roomIdError = 'Room ID should only consist of letters';
		}
		return validName && validRoomId;
	}
}