angular
	.module('chatroom')
	.controller('chatController',['$scope', '$rootScope', '$location' ,'$routeParams', 'dataService', chatController])
	.run( function($rootScope, $routeParams, $location) { //login validation
	    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
	      if ( $rootScope.name == null) {
	          $location.path( "/" );
	    	}         
	    });
	});

function chatController(scope,rootScope,location,routeParams,dataService){
	scope.roomId = routeParams.roomId;
	scope.message = '';
	scope.messageList = [];

	scope.messages = new Firebase(dataService.getFirebaseUrl()+'messages/' + scope.roomId + '/');

	scope.sendMessage = function(){
		scope.messages.push({
			name:rootScope.name,
			message:scope.message
		});
		scope.message = '';
	}

	// Retrieve new posts as they are added to our database
	scope.messages.limitToLast(10).on("child_added", function(snapshot) {
		var newPost = snapshot.val();
		rootScope.$evalAsync(
			function handleEvalAsync(){
				scope.messageList.push({"key":snapshot.key(),"name":newPost.name, "message":newPost.message});
			}
		);
	});

	//watch if list size goes above limit then remove top
	scope.$watch('messageList.length', 
        function(newValue, oldValue){
            if(newValue > 100)
               scope.messageList.shift();
        }
    );
}